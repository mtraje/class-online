<?php include("nocache.php"); ?>

<!DOCTYPE html>

<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Pre-Purchase Request Approval">
    <meta name="author" content="Michael M. Traje">
    <meta name="google-signin-client_id" content="947441803986-8c9gpc8m8l3bhp8spo3ib548ehqea14m.apps.googleusercontent.com">
   
    <link rel="icon" href="../../favicon.ico">

    <title>Online Grading System</title>

    <script src="https://www.gstatic.com/firebasejs/4.11.0/firebase.js"></script>

    <script src="https://www.gstatic.com/firebasejs/4.10.1/firebase-app.js"></script>
    <script src="https://www.gstatic.com/firebasejs/4.10.1/firebase-auth.js"></script>
    
    
    <script src="init.js"></script>
    
    <!-- Bootstrap core CSS 
    <link href="node_modules/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet"> -->
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet">
     <!-- Custom CSS -->
        <!-- Custom Fonts -->

     <link href="css/stylish-portfolio.min.css" rel="stylesheet">
    <!--
    <script src="node_modules\jquery\dist\jquery.min.js"></script> -->
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"
			integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
			crossorigin="anonymous"></script>

  
    <link href="css/heroic-features.css" rel="stylesheet">
    <script type="text/javascript">
        initApp = function () {
            var provider = new firebase.auth.GoogleAuthProvider();


           /*  firebase.auth().signInWithPopup(provider).then(function(result) {
                // This gives you a Google Access Token. You can use it to access the Google API.
                var token = result.credential.accessToken;
                        // The signed-in user info.
                var user = result.user;
                        
                        $( "#navobj" ).fadeIn();
                        $( "#mainObj" ).fadeIn();
                
                        $( "#uluser").text('Welcome to your page, ' + user.email);
                        //alert(user.e  mail);
                        //onSignIn(user.email);

                

                        // ...
                        }).catch(function(error) {
                        // Handle Errors here.
                        var errorCode = error.code;
                        var errorMessage = error.message;
                        // The email of the user's account used.
                        var email = error.email;
                        // The firebase.auth.AuthCredential type that was used.
                        var credential = error.credential;
                    // ...
            }); */

            function onSignIn(googleUser) {

            
                const profile = googleUser.getBasicProfile();

                console.log('Google Auth Response', googleUser);
                // We need to register an Observer on Firebase Auth to make sure auth is initialized.
                var unsubscribe = firebase.auth().onAuthStateChanged(function(firebaseUser) {
                    unsubscribe();
                    // Check if we are already signed-in Firebase with the correct user.
                    if (!isUserEqual(googleUser, firebaseUser)) {
                    // Build Firebase credential with the Google ID token.
                    var credential = firebase.auth.GoogleAuthProvider.credential(
                        googleUser.getAuthResponse().id_token);
                    // Sign in with credential from the Google user.
                    firebase.auth().signInWithCredential(credential).catch(function(error) {
                        // Handle Errors here.
                        var errorCode = error.code;
                        var errorMessage = error.message;
                        // The email of the user's account used.
                        var email = error.email;
                        // The firebase.auth.AuthCredential type that was used.
                        var credential = error.credential;
                        // ...
                    });
                    } else {
                    console.log('User already signed-in Firebase.');
                    }
                });
            }

            function isUserEqual(googleUser, firebaseUser) {
            if (firebaseUser) {
                var providerData = firebaseUser.providerData;
                for (var i = 0; i < providerData.length; i++) {
                if (providerData[i].providerId === firebase.auth.GoogleAuthProvider.PROVIDER_ID &&
                    providerData[i].uid === googleUser.getBasicProfile().getId()) {
                    // We don't need to reauth the Firebase connection.
               
                    return true;
                }
                }
            }
            return false;
}
            
        };

        window.addEventListener('load', function () {
            initApp()
        }); 
    </script>
    <style>
      .top-buffer {
        margin-top: 100px;
      }
      .masthead {
        margin: 10px, 10px, 10px, 10px;
      }
    </style>
</head>

<body class="bg-primary"> 
    <!-- Navigation -->
    <nav id="navobj" style="display: none;" class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
      <div class="container">
        <a class="navbar-brand" href="#">Online Grading System</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav ml-auto">
          <li class="nav-item">
              <a class="nav-link active" onclick="" href="#">School Year 2018-2019
                <span class="sr-only">(current)</span>
              </a>
            </li>
            <li class="nav-item active">
              <a class="nav-link active" onclick="SignOut();" href="#">Sign out
                <span class="sr-only">(current)</span>
              </a>
            </li>
   
          </ul>
        </div>
      </div>
    </nav>


    <div id="mainObj" style="display: none;" class="container bg-primary text-center">

      <div class="row text-center top-buffer">

        <div class="col-lg-3 col-md-6 mb-4">
          <div class="card">
            <img class="card-img-top" src="img/cardbanner-1.png" alt="">
            <div class="card-body">
              <h4 class="card-title">Grade Levels</h4>
             <p class="card-text">List of assigned classes along with their respective class schedule </p>
            </div>
            <div class="card-footer">
              <a href="#" class="btn btn-primary btn-block">View</a>
            </div>
          </div>
        </div>

         <div class="col-lg-3 col-md-6 mb-4">
          <div class="card">
          <img class="card-img-top" src="img/cardbanner-1.png" alt="">
            <div class="card-body">
              <h4 class="card-title">Students</h4>
             <p class="card-text">List of students in alphabetical order.</p>
            </div>
            <div class="card-footer">
              <a href="#" class="btn btn-primary btn-block">View</a>
            </div>
          </div>
        </div>

        <div class="col-lg-3 col-md-6 mb-4">
          <div class="card">
          <img class="card-img-top" src="img/cardbanner-1.png" alt="">
            <div class="card-body">
              <h4 class="card-title">My Profile</h4>
             <p class="card-text">View and edit my profile.</p>
            </div>
            <div class="card-footer">
              <a href="#" class="btn btn-primary btn-block">View</a>
            </div>
          </div>
        </div>

        <div class="col-lg-3 col-md-6 mb-4">
          <div class="card">
          <img class="card-img-top" src="img/cardbanner-1.png" alt="">  
            <div class="card-body">
              <h4 class="card-title">Holidays / Announcements</h4>
             <p class="card-text">View updates on class schedules and announcements </p>
            </div>
            <div class="card-footer">
              <a href="#" class="btn btn-primary btn-block">View</a>
            </div>
          </div>
        </div>
    </div>
    <!-- /.container -->

    <div id="logoutmsg" class="card" style="display: none;"> 
        <div class="card-body">
            <h4 class="card-title">Thank you! Bye!</h4>
            <p class="card-text">Reload page to login again.</p>
        </div>
        <div class="card-footer">
            <a href="#" class="btn btn-primary btn-block">View</a>
        </div>
        </div>
    </div>
  
          

    <nav id="navobj2" class="navbar navbar-expand-lg navbar-dark bg-dark fixed-bottom text-white">
      <div class="container">
       <p id="uluser"></p>
      </div>
    </nav>

   
    <script>
        
       
        function SignOut() {
         
            
            firebase.auth().signOut().then(function() {
            // Sign-out successful.
                
                $( "#navobj" ).fadeOut();
                $( "#mainObj" ).fadeOut();
                $( "#navobj2" ).fadeOut();
           
            }).catch(function(error) {
                alert('Sign out Failed');
            });
        }

     
    </script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>

    <script src="https://maxcdn.bootstrapcdn.com/js/ie10-viewport-bug-workaround.js"></script>
</body>

</html>