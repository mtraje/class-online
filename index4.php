<?php include("nocache.php"); ?>
<?php include("db_settings.php"); ?>
<?php 

//define('MYSQL_ASSOC',MYSQLI_ASSOC);

    if (isset($_POST["temailko"]) && !empty($_POST["temailko"])) {
        $temail = $_POST["temailko"];
    }else{  
        header('Location: main.php');
    }

?>

<?php 

  $myhash = "";
  $myname = "";
  $myrole = "";

  if ($result5 = $db->query("SELECT t_idhash, t_name, t_role FROM rec_teacher WHERE t_email = '".$temail."';")) {
    while ($row5 = $result5->fetch_array(MYSQLI_ASSOC)):
        $myhash = $row5["t_idhash"];
        $myname = $row5["t_name"];
        $myrole = $row5["t_role"];
    endwhile;
  }

  switch ($myrole) {
    case 1:
        $mytrole = "Math Teacher";
        break;
    case 2:
        $mytrole = "Class Adviser";
        break;
    case 3:
        $mytrole = "Area Head";
        break;
    case 4:
        $mytrole = "System Admin";
        break;
    case 5:
        $mytrole = "Registrar";
        break;
    default:
        $mytrole = "Online Grading System";
}

?>



<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <title>Online Grading System</title>

  <script src="https://www.gstatic.com/firebasejs/4.9.0/firebase-app.js"></script>
  <script src="https://www.gstatic.com/firebasejs/4.9.0/firebase-auth.js"></script>

   <script src="init.js"></script>

  <link href="bvendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <!-- Custom fonts for this template-->
  <link href="bvendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

  <link href='https://fonts.googleapis.com/css?family=Alegreya Sans' rel='stylesheet'>
  <!-- Custom styles for this template-->
   <link href="bvendor/datatables/dataTables.bootstrap4.css" rel="stylesheet"> 
  <link href="css/sb-admin.css" rel="stylesheet">

  <script src="gsecurity.js"></script>
  <script src="sidebarscript.js"></script>
 

  <style>
      .card {
        height: 100%;
          margin: 0 auto; /* Added */
        float: none; /* Added */
        margin-bottom: 10px; 
      }

      .row{
        
        margin-bottom: 0px
      }
      .button {
        
      }
  </style>

</head>

<body class="fixed-nav sticky-footer bg-dark" id="page-top">
  <!-- Navigation-->
  <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top" id="mainNav">
    <a class="navbar-brand" href="index.php"><?php echo $mytrole; ?></a>
    
    <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    
    <div class="collapse navbar-collapse" id="navbarResponsive">
    <?php

    switch ($myrole) {
      case 1:
          include("sidebar.php"); 
          break;
      case 2:
          include("sidebar_adv.php"); 
          break;
      case 3:
          include("sidebar_arh.php"); 
          break;
      case 4:
          include("sidebar_sys.php"); 
          break;   
      default: 
    }  
        
    ?>

        
      </ul>
      <ul class="navbar-nav sidenav-toggler">
        <li class="nav-item">
          <a class="nav-link text-center" id="sidenavToggler">
            <i class="fa fa-fw fa-angle-left"></i>
          </a>
        </li>
      </ul>
      <?php include("nav.php"); ?>
      
      
    </div>
  </nav>
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumbs-->


<?php
    switch ($myrole) {
      case 1:
?>

      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="#" onClick="changeurl();">Main</a>
        </li>
        <li id="bcrumb" class="breadcrumb-item active">For Editing</li>
      </ol>
<?php
      break;
      case 2:
?>

      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="#" onClick="changeurl();">Main</a>
        </li>
        <li id="bcrumb" class="breadcrumb-item active">For Editing</li>
      </ol>

<?php
          break;
      case 3:

          break;    

      default: 
    } 
 ?>   






      <div class="row">
        <div id="mycontent" class="col-12">
               <?php include("dashboard.php"); ?>
        </div>
      </div>
    </div>
    <!-- /.container-fluid-->
    <!-- /.content-wrapper-->
    <footer class="sticky-footer">
      <div class="container">
        <div class="text-center">
          <small><span id="logusr"><?php echo $temail; ?></span> - <span id="nameusr"><?php echo $myname; ?></span></small>
        </div>
      </div>
    </footer>
    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
      <i class="fa fa-angle-up"></i>
    </a>
     <?php include("modalviews.php"); ?>
    <script>
      function SignOut() {
          window.open(
            'http://mail.google.com',
            '_blank'
          );
          firebase.auth().signOut().then(function () {
            // Sign-out successful.
            window.top.close();
            
          }).catch(function (error) {
            alert('Sign out Failed');
          });
      }
            function ClassAdv() {

                   $('#bcrumb').text('My Advisory Class');
                   $("#mycontent").load("advisorycls.php", {
                       name: $("#logusr").text()
                       //name: teacherstr
                   });

            }
            function SubjTeach() {

                   $('#bcrumb').text('My Advisory Class');
                   $("#mycontent").load("subjectTeacher.php", {
                       name: $("#logusr").text()
                       //name: teacherstr
                   });

            }
            function AdvTeach() {

                   $('#bcrumb').text('My Advisory Class');
                   $("#mycontent").load("AdvTeacher.php", {
                       name: $("#logusr").text()
                       //name: teacherstr
                   });

            }
            function forviewing() {
                $('#bcrumb').text('For Editing');
                $("#mycontent").load("forviewing_adv.php");
            }
            function forediting() {
                $('#bcrumb').text('For Viewing');
                $("#mycontent").load("forediting_adv.php");
            }

             function forviewing_math() {
                $('#bcrumb').text('For Editing');
                $("#mycontent").load("math.php");
            }
    </script>

      <!-- Bootstrap core JavaScript-->
    <script src="bvendor/jquery/jquery.min.js"></script>
    <script src="bvendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- Core plugin JavaScript-->
    <script src="bvendor/jquery-easing/jquery.easing.min.js"></script>
    <!-- Page level plugin JavaScript-->
 <!--    <script src="vendor/datatables/jquery.dataTables.js"></script>
    <script src="vendor/datatables/dataTables.bootstrap4.js"></script> -->
    <!-- Custom scripts for all pages-->
    <script src="js/sb-admin.min.js"></script>
    <!-- Custom scripts for this page-->
 <!--    <script src="js/sb-admin-datatables.min.js"></script> -->
  </div>
</body>

</html>
