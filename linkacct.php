<?php include("nocache.php"); ?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="Michael Traje">
  <title>Online Grading System</title>
  <!-- Bootstrap core CSS-->

    <script src="https://www.gstatic.com/firebasejs/4.9.0/firebase-app.js"></script>
    <script src="https://www.gstatic.com/firebasejs/4.9.0/firebase-auth.js"></script>

    <script src="init.js"></script>
     <script type="text/javascript">

            initApp = function () {
                

                firebase.auth().onAuthStateChanged(function (user) {
                if (user) {
                      var useremail = user.email;
        

                    alert(useremail);
                } else {
                    //var mytoken = localStorage.getItem('access_token')
                    //alert("https://accounts.google.com/o/oauth2/revoke?token=" + mytoken)
                    
                    window.location = "login-google.php";

                }
                }, function (error) {
                console.log(error);
                });
            };
                    var provider = new firebase.auth.GoogleAuthProvider();

            function linkmyAcct() {
                
                    // $("#teacherInputEmail1").text(user.email);
                       auth.currentUser.linkWithPopup(provider).then(function(result) {
                        // Accounts successfully linked.
                        var credential = result.credential;
                        var user = result.user;
                        // ...
                        alert('success');
                        }).catch(function(error) {
                        // Handle Errors here.
                        // ...
                        });
            }  

            function LoginClick(){
       
                firebase.auth().signInWithPopup(provider).then(function(result) {

                    var user = result.user;
                    
                    auth.currentUser.linkWithPopup(provider).then(function(result) {
                    var credential = result.credential;
                        var user = result.user;
                        // ...
                        alert('success');
                    }).catch(function(error) {
                        // Handle Errors here.
                        // ...
                    });         


                }).catch(function(error) {
                        // Handle Errors here.
                        var errorCode = error.code;
                        var errorMessage = error.message;
                        // The email of the user's account used.
                        var email = error.email;
                        // The firebase.auth.AuthCredential type that was used.
                        var credential = error.credential;
                    // ...
                });

            }    

            window.addEventListener('load', function () {
                initApp()
                
            });

 </script>

  <link href="bvendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <!-- Custom fonts for this template-->
  <link href="bvendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <!-- Custom styles for this template-->
  <link href="css/sb-admin.css" rel="stylesheet">
</head>

<body class="bg-dark">
<span id="teacherInputEmail1" style="display: none;"></span>
  <div id="ogscontainer" class="container"> 
       <a id="btnSbmt" class="btn btn-primary btn-block" href="#"  onClick="linkmyAcct();">Link</a>
  </div>
  <!-- Bootstrap core JavaScript-->
  <script src="bvendor/jquery/jquery.min.js"></script>
  <script src="bvendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <!-- Core plugin JavaScript-->
  <script src="bvendor/jquery-easing/jquery.easing.min.js"></script>
</body>
</html>