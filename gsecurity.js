initApp = function () {

    checkLoginStatus();
    

    function checkLoginStatus() {
        firebase.auth().onAuthStateChanged(function (user) {
          if (user) {
            $('#logusr').text(user.email);
        
          } else {
            window.location = "login.php";
          }
        }), function (error) {
          console.log(error);
        };
    };
    }

    function SignOutFire() {

      firebase.auth2().disconnect();
      firebase.auth().disconnect();
      firebase.auth().signOut();

    };

    function updateTime(){
              var petsam = moment().format('LL');
              document.getElementById('petsa').textContent = petsam;
    }

    

  window.addEventListener('load', function () {
    initApp()
    
  });
