<?php include("nocache.php"); ?>
<?php include("db_settings.php"); ?>

<?php

$temail = $_POST["name"];

 $sql = "select * from vw_adviser_sum where s_adviser = '" . $temail . "'";

?>

<div class="card mb-3">
        <div class="card-header">
          <i class="fa fa-table"></i> View Summary of my Advisory Class</div>
        <div class="card-body">
          <div class="table-responsive table-striped">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
              <thead>
                <tr>
        
                
                  <th>Grade</th>
                  <th>Section</th>
                   <th>URL</th>

                </tr>
              </thead>
              <tfoot>
                <tr>      
                   <th>Grade</th>
                  <th>Section</th>
                   <th>URL</th>

                </tr>
              </tfoot>
              <tbody>
             
<?php

              if ($result5 = $db->query($sql)) {
                while ($row5 = $result5->fetch_array(MYSQLI_ASSOC)):
?>
                <tr>
                <td><?php echo "Grade " . $row5["grade"]; ?></td>
                
                <td><?php echo $row5["sectionname"]; ?></td>
            
                  <td><a href="<?php echo $row5["adv_url"]; ?>" target="_blank" class="btn btn-primary btn-small" role="button" aria-pressed="true">Grade Summary</a> <a href="<?php echo $row5["url_con4"]; ?>" target="_blank" class="btn btn-success btn-small" role="button" aria-pressed="true">Conduct Summary</a></td>              
                </tr>
<?php
                endwhile;
              }
?>
              </tbody>
            </table>
          </div>
        </div>
        <div class="card-footer small text-muted"></div>
    </div>
</div>

    <script src="bvendor/datatables/jquery.dataTables.js"></script>
    <script src="bvendor/datatables/dataTables.bootstrap4.js"></script>
 
    <script src="js/sb-admin-datatables.min.js"></script>