function execscript() {
    RemoveData();
    putData();
}

function putData() {
    var conn = Jdbc.getConnection('jdbc:mysql://35.194.253.74:3306/grading_schema', 'admgradesys', 'adm2018grades');


    var stmt = conn.createStatement();

    var doc = SpreadsheetApp.openById("1EHcp3-P934irz5_3neLJujVx9PLpmeyGXGzx7nwKgzU");
    var sheet = doc.getSheets()[0];

    var data = sheet.getRange("SUM!B11:K26").getValues();

    for (var i = 0; i < data.length; i++) {

        var sql = "INSERT INTO test (testname, math, english, conduct, conduct2, genavedec, genavernd) VALUES ('" + data[i][0] + "','" + data[i][4] + "','" + data[i][5] + "','" + data[i][6] + "','" + data[i][7] + "','" + data[i][8] + "','" + data[i][9] + "')";
        var count = stmt.executeUpdate(sql, 1)
    }

    stmt.close();
    conn.close();
}


function RankData() {
    var conn = Jdbc.getConnection('jdbc:mysql://35.194.253.74:3306/grading_schema', 'admgradesys', 'adm2018grades');

    var stmt = conn.createStatement();
    var start = new Date();

    var rs = stmt.executeQuery('select testname, "" AS placeholder1, "" AS placeholder2, "" AS placeholder3, math, english, conduct, conduct2, genavedec, genavernd from test order by test.genavernd DESC');

    var doc = SpreadsheetApp.openById("1R6bDC-rNCoskT8GsvHERWsGhl2OeA4LfWDj1iX_zX0o");
    var cell = doc.getRange("B11");

    var row = 0;

    while (rs.next()) {
        for (var col = 0; col < rs.getMetaData().getColumnCount(); col++) {
            cell.offset(row, col).setValue(rs.getString(col + 1));
            //cell.setValue(rs.getString(col + 1));
        }
        row++;
    }


    rs.close();
    stmt.close();
    conn.close();
    var end = new Date(); // Get script ending time
    Logger.log('Time elapsed: ' + (end.getTime() - start.getTime()));


}


function RemoveData() {
    var conn = Jdbc.getConnection('jdbc:mysql://35.194.253.74:3306/grading_schema', 'admgradesys', 'adm2018grades');
    conn.createStatement().execute('DELETE FROM test;');
}
