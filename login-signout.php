<?php include("nocache.php"); ?>

<!DOCTYPE html>

<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Online Grading System">
    <meta name="author" content="Michael M. Traje">
    <meta name="google-signin-client_id" content="947441803986-8c9gpc8m8l3bhp8spo3ib548ehqea14m.apps.googleusercontent.com">
   
    <link rel="icon" href="../../favicon.ico">

    <title>Online Grading System</title>

    <script src="https://www.gstatic.com/firebasejs/4.11.0/firebase.js"></script>

    <script src="https://www.gstatic.com/firebasejs/4.10.1/firebase-app.js"></script>
    <script src="https://www.gstatic.com/firebasejs/4.10.1/firebase-auth.js"></script>
    
    
    <script src="init.js"></script>
    
    <!-- Bootstrap core CSS -->
<!--     <link href="node_modules/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet"> 
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet"> -->
     <!-- Custom CSS -->
        <!-- Custom Fonts -->

    <!--
    <script src="node_modules\jquery\dist\jquery.min.js"></script> -->
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"
			integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
			crossorigin="anonymous"></script>

    
    <link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="//netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js"></script>
    <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
    

    <script type="text/javascript">
        initApp = function () {
           

            firebase.auth().onAuthStateChanged(function (user) {
                if (user) {
                    //alert(user.email);
                    window.location = "index.php";
                } else {
                    
                }
                }), function (error) {
                    console.log(error);
                    alert(error);
                };
            };

            function checkifaccountexist() {
                
            }

            function LoginClick(){
                firebase.auth().signOut().then(function() {
                // Sign-out successful.
                    alert('signout success');
                }).catch(function(error) {
                // An error happened.
                });
            }

        window.addEventListener('load', function () {
            initApp()
        }); 
    </script>
    <style>
        .form-signin
        {
            max-width: 330px;
            padding: 15px;
            margin: 0 auto;
        }
        .form-signin .form-signin-heading, .form-signin .checkbox
        {
            margin-bottom: 10px;
        }
        .form-signin .checkbox
        {
            font-weight: normal;
        }
        .form-signin .form-control
        {
            position: relative;
            font-size: 16px;
            height: auto;
            padding: 10px;
            -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
            box-sizing: border-box;
        }
        .form-signin .form-control:focus
        {
            z-index: 2;
        }
        .form-signin input[type="email"]
        {
            margin-bottom: 5px;
            border-bottom-left-radius: 0;
            border-bottom-right-radius: 0;
        }
        .form-signin input[type="password"]
        {
            margin-bottom: 10px;
            border-top-left-radius: 0;
            border-top-right-radius: 0;
          
        }
        .account-wall
        {
            margin-top: 100px;
            padding: 20px 0px 20px 0px;
            background-color: #f7f7f7;
            -moz-box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);
            -webkit-box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);
            box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);
        }
        .login-title
        {
            color: #F0EDF5;
            font-size: 18px;
            font-weight: 400;
            display: block;
        }
        .profile-img
        {
            width: 120px;
            height: 120px;
            margin: 0 auto 10px;
            display: block;
            -moz-border-radius: 50%;
            -webkit-border-radius: 50%;
            border-radius: 50%;
        }
        .need-help
        {
            margin-top: 10px;
        }
        .new-account
        {
            display: block;
          
        }
        body {

            background-color: #1d809f;
        }

    </style>
</head>
<body> 
    <div class="container">
        <div class="row">
            <div class="col-sm-6 col-md-4 col-md-offset-4">
            
                <div class="account-wall">
                    <h1 id="mytitle" class="text-center login-title" style="color: green;">ONLINE GRADING SYSTEM (OGS)</h1><br />
                    <img class="profile-img" src="img/unlock.png?sz=120"
                        alt="">
                    <form class="form-signin">
                   
                    <span id="reauth-email" class="reauth-email"></span>
                             <!-- <label for="inputEmail" class="sr-only">Email address</label> -->
                            <!-- <input type="email" id="inputUser" class="form-control" placeholder="Email address" required autofocus>
                            <label for="inputPassword" class="sr-only">Password</label>
                            <input type="password" id="inputPassword" class="form-control" placeholder="Password" required> -->
                                    
                      
                            <button class="btn btn-lg btn-success btn-block" type="button" onClick="LoginClick();">Sign Out</button>
                 

                        <a href="#" class="pull-right need-help">Need help? </a><span class="clearfix"></span>
                    </form>
                </div>
            
            </div>
        </div>
    </div>

    <script src="https://maxcdn.bootstrapcdn.com/js/ie10-viewport-bug-workaround.js"></script>
</body>

</html>