<?php include("nocache.php"); ?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="Michael Traje">
  <title>Online Grading System</title>
  <!-- Bootstrap core CSS-->

  <link href="css/profileindex.css" rel="stylesheet">


    <script src="https://www.gstatic.com/firebasejs/4.9.0/firebase-app.js"></script>
    <script src="https://www.gstatic.com/firebasejs/4.9.0/firebase-auth.js"></script>
    <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
    <script src="init.js"></script>
     <script type="text/javascript">

            initApp = function () {

                firebase.auth().onAuthStateChanged(function (user) {
                if (user) {
                      var useremail = user.email;
                    // $("#teacherInputEmail1").text(user.email);
             


                      $.post("validate.php", {
                         usremail: useremail
                      },
                      function (data, status) {
                          if(data == 1) {
                              $("#ogscontainer").load("worksheets.php", {
                                   usremail: useremail
                              });
                          }
                          else
                          {
                              $("#ogscontainer").load("denied.php");
                          }
                      });
                } else {
                    //var mytoken = localStorage.getItem('access_token')
                    //alert("https://accounts.google.com/o/oauth2/revoke?token=" + mytoken)
                    var mytoken = localStorage.getItem('lsEmail');
                    window.location = "login-google.php";

                }
                }, function (error) {
                console.log(error);
                });
            };
            function Logmeout() {

              
              var wingog = window.open("https://accounts.google.com/Logout", "_blank");
            
  
              firebase.auth().signOut().then(function() {
                
                // Sign-out successful.
              }).catch(function(error) {
                // An error happened.
              });
            
            }

    
            function LoginClick(){
                var provider = new firebase.auth.GoogleAuthProvider();

                firebase.auth().currentUser.linkWithPopup(provider).then(function(result) {
                  // Accounts successfully linked.
                  var credential = result.credential;
                  var user = result.user;
                  // ...
                }).catch(function(error) {
                  // Handle Errors here.
                  // ...
                  alert(error);
                });
            }   

            function checkschool(myemail) {
                var c  = $("#cmbschool" ).val();
         
                 $("#ogs_gradelevel").load("gradelevel_worksheet.php", {
                                   usremail: myemail,
                                   usrschool: c
                  });
                

            }

            function change_section(myemail, myschool) {
                var c  = $("#cmbgrade" ).val();

                  $("#ogs_section").load("section_worksheet.php", {
                                   usremail: myemail,
                                   usrschool: myschool,
                                   usrgrade: c
                  });
            }

            function show_wb(myemail) {
                var mysection = $("#cmbsection" ).val();

                 $("#ogs_sheets").load("avail_worksheets.php", {
                                   usremail: myemail,
                                   usrsection: mysection
                  });
            }
            window.addEventListener('load', function () {
                initApp()
                
            });

 </script>

  <link href="bvendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <!-- Custom fonts for this template-->
  <link href="bvendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <!-- Custom styles for this template-->
  <link href="css/sb-admin.css" rel="stylesheet">
</head>

<body class="bg-white">
  <div class="container">
		<div class="col-md-9">
   
                  <div id="ogscontainer" class="container"> </div>
		</div>
	</div>

<!-- <span id="teacherInputEmail1" style="display: none;"></span>
  <div id="ogscontainer" class="container"> </div> -->
  <!-- Bootstrap core JavaScript-->
<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>

</body>
</html>