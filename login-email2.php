<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Pre-Purchase Request Approval">
    <meta name="author" content="Michael M. Traje">
    <link rel="icon" href="../../favicon.ico">

    <title>Purchase Request Login Form</title>

    <script src="https://www.gstatic.com/firebasejs/4.9.0/firebase-app.js"></script>
    <script src="https://www.gstatic.com/firebasejs/4.9.0/firebase-auth.js"></script>
    
    <!-- Bootstrap core CSS 
    <link href="node_modules/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet"> -->
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet">
    
    <!--
    <script src="node_modules\jquery\dist\jquery.min.js"></script> -->
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"
			integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
			crossorigin="anonymous"></script>

    
    <script src="init.js"></script>

    <link href="signin.css" rel="stylesheet"> 

    <script type="text/javascript">
        initApp = function () {
            firebase.auth().onAuthStateChanged(function (user) {
                if (user) {
                    // User is signed in.
                    window.location = "dashboard.php";
                    firebase.signInSuccess()
                } else {
                    $( "#mf" ).fadeIn(2000);
    
                }
            }, function (error) {
                console.log(error);
            });
        };

        window.addEventListener('load', function () {
            initApp()
        }); 
    </script>

</head>

<body  class="text-center"> 
    <div id="mf" class="container" style="display:none">
                 <div class="text-center mb-4">
              <img class="mb-4" src="images/CamSurOfficialSeal.png" alt="" width="160" height="160">
              
              <h3 class="text-muted">Pre-PR Clearance</h3>
            </div>
        <div id="myform" class="form-signin" action="#">
      
                <label for="inputEmail" class="sr-only">Email address</label>
                <input type="email" id="inputUser" class="form-control" placeholder="Email address" required autofocus>
                <label for="inputPassword" class="sr-only">Password</label>
                <input type="password" id="inputPassword" class="form-control" placeholder="Password" required>
              
                <button id="btnSbmt" class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
              
              <?php echo $alert_message ?> 
            <p class="mt-5 mb-1 text-muted text-center">Provincial Internal Audit Office</p>
            <p class="text-muted text-center">&copy; 2018-2019</p>

        </div>
    </div>
    <span id="result"></span>
    <script>
        $(document).ready(function () {
            $("#mess").hide();
            $("#correct").hide();

            $("#btnSbmt").click(function () {
                var email = $("#inputUser").val();
                var password = $("#inputPassword").val();
                firebase.auth().signInWithEmailAndPassword(email, password).catch(function (error) {
                    // Handle Errors here.
                    var errorCode = error.code;
                    var errorMessage = error.message;
                    // [START_EXCLUDE]
                    if (errorCode === 'auth/wrong-password') {
                        alert('Wrong Password');
                    } else {
                        alert(errorMessage);

                    }
                    //console.log(error);
                    console.log(error.code);


                });
            });
        });
    </script>
    <!--
    <script src="js/ie10-viewport-bug-workaround.js"></script>
    -->
    <script src="https://maxcdn.bootstrapcdn.com/js/ie10-viewport-bug-workaround.js"></script>
</body>

</html>