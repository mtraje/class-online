<?php include("nocache.php"); ?>

<?php
function getUserIP()
{
    // Get real visitor IP behind CloudFlare network
    if (isset($_SERVER["HTTP_CF_CONNECTING_IP"])) {
              $_SERVER['REMOTE_ADDR'] = $_SERVER["HTTP_CF_CONNECTING_IP"];
              $_SERVER['HTTP_CLIENT_IP'] = $_SERVER["HTTP_CF_CONNECTING_IP"];
    }
    $client  = @$_SERVER['HTTP_CLIENT_IP'];
    $forward = @$_SERVER['HTTP_X_FORWARDED_FOR'];
    $remote  = $_SERVER['REMOTE_ADDR'];

    if(filter_var($client, FILTER_VALIDATE_IP))
    {
        $ip = $client;
    }
    elseif(filter_var($forward, FILTER_VALIDATE_IP))
    {
        $ip = $forward;
    }
    else
    {
        $ip = $remote;
    }

    return $ip;
}


$user_ip = getUserIP();

 // Output IP address [Ex: 177.87.193.134]


?>

<div class="card mt-lg-5 text-white">
	<div class="card-header bg-danger">TEACHER PROFILE</div>
		<div class="card-body">
			<div class="col-lg-12">
				<div class="row">
					<div class="col-lg-3 mb-3">
						<img src="img/denied.jpg" height="150px" width="150px" >
					</div>

					<div class="col-lg-9">
						<h1 class="text-dark mb-0">You do not have access to the requested page</h1>
						<h5 class="text-dark mt-0">Your IP Address is logged: <?php echo $user_ip;  ?></h5>
					</div>
                                </div>
                     


                                

                                <div class="text-right">
                                        
                                        <a id="btnSbmt" class="btn btn-danger pull-right" href="#"  onClick="Logmeout();">Sign out from Google</a>
                                </div>
                        </div>            
                </div>
        </div>
</div>
